import pandas as pd

left = pd.read_csv(r'AirbnbChicagoOutput.csv')

CheckInOut = list(left['Check-in & Check-out'])
Descriptionkeyword = list(left['Description keyword'])
Rules = list(left['Rules & Regulations'])
Social = list(left['Social Media'])
Neighbourhoodkeyword = list(left['Neighbourhood keyword'])
Amenities = list(left['Amenities'])

def word2vec(word):
    from collections import Counter
    from math import sqrt
    # count the characters in word
    cw = Counter(word)
    # precomputes a set of the different characters
    sw = set(cw)
    # precomputes the "length" of the word vector
    lw = sqrt(sum(c*c for c in cw.values()))

    # return a tuple
    return cw, sw, lw

def cosdis(v1, v2):
    # which characters are common to the two words?
    common = v1[1].intersection(v2[1])
    # by definition of cosine distance we have
    return sum(v1[0][ch]*v2[0][ch] for ch in common)/v1[2]/v2[2]

list_A = Descriptionkeyword
list_B = Amenities
keys = []
words = []
ress = []
threshold = 0.80     # if needed
for i,j in zip(list_A, list_B):
        try:
            # print(key)
            # print(word)
            res = cosdis(word2vec(i), word2vec(j))
            # print(res)
            #print("The cosine similarity between : {} and : {} is: {}".format(word, key, res*100))
            keys.append(i)
            words.append(j)
            ress.append(res*100)
        except IndexError:
            pass

left['AmenitiesSimilarity'] = ress

list_A = Descriptionkeyword
list_B = CheckInOut
keys = []
words = []
ress = []
threshold = 0.80     # if needed
for i,j in zip(list_A, list_B):
        try:
            # print(key)
            # print(word)
            res = cosdis(word2vec(i), word2vec(j))
            # print(res)
            #print("The cosine similarity between : {} and : {} is: {}".format(word, key, res*100))
            keys.append(i)
            words.append(j)
            ress.append(res*100)
        except IndexError:
            pass

left['CheckInOutsimilarity'] = ress

list_B = Rules
keys = []
words = []
ress = []
threshold = 0.80     # if needed
for i,j in zip(list_A, list_B):
        try:
            # print(key)
            # print(word)
            res = cosdis(word2vec(i), word2vec(j))
            # print(res)
            #print("The cosine similarity between : {} and : {} is: {}".format(word, key, res*100))
            keys.append(i)
            words.append(j)
            ress.append(res*100)
        except IndexError:
            pass

left['Rulessimilarity'] = ress

list_B = Social
keys = []
words = []
ress = []
threshold = 0.80     # if needed
for i,j in zip(list_A, list_B):
        try:
            # print(key)
            # print(word)
            res = cosdis(word2vec(i), word2vec(j))
            # print(res)
            #print("The cosine similarity between : {} and : {} is: {}".format(word, key, res*100))
            keys.append(i)
            words.append(j)
            ress.append(res*100)
        except IndexError:
            pass

left['SocialMediasimilarity'] = ress

list_B = Neighbourhoodkeyword
keys = []
words = []
ress = []
threshold = 0.80     # if needed
for i,j in zip(list_A, list_B):
        try:
            # print(key)
            # print(word)
            res = cosdis(word2vec(i), word2vec(j))
            # print(res)
            #print("The cosine similarity between : {} and : {} is: {}".format(word, key, res*100))
            keys.append(i)
            words.append(j)
            ress.append(res*100)
        except IndexError:
            pass

left['Neighbourhoodsimilarity'] = ress


left.to_csv(r'AirbnbChicagoOutput.csv', index=False)