
# Airbnb listing descriptions: What Hosts need to know

![image.png](./image.png)

Understanding the impact of an Airbnb listing’s description on its performance.


## Authors

- [@Aaron Chen](chen4065@purdue.edu)
- [@Gagan Pahuja](gpahuja@purdue.edu)
- [@Puja Gupta](gupta714@purdue.edu)
- [@Rupal Bilaiya](rbilaiya@purdue.edu)



## Acknowledgements

 - [Designing a better Airbnb experience](https://medium.com/@nicolelianjf/ui-ux-case-study-designing-a-better-airbnb-experience-d502d72133f1)
 - [Sprucing up your listing description](https://www.airbnb.com/resources/hosting-homes/a/sprucing-up-your-listing-description-13#:~:text=A%20great%20listing%20description%20is,ll%20find%20when%20they%20arrive.)
 - [Chicago default Airbnb dataset](http://insideairbnb.com/get-the-data.html)


## Tech Stack

**Language:** Python

**Tools:** Pycharm, Excel, Minitab


## Code Description

All the python py files should be executed in the same order as present below:

#### Scrapping.py

The code is used to scrap the data from Airbnb site. We are scrapping complete description, check-in check-out time and rules. Once the output file is generated, we will get neighborhood_overview and Amenities for corresponding listings from original file. Also add standard check-in check-out, social media and rules list for further analysis. Finally we will get the target variables Average Rating and derived column Occupancy Rate.

###### Input File - listingsChicago.csv
###### Output File - AirBnbData.csv

#### TokenizeCode.py

We need to tokenize Description and Neighborhood_Overview column, so that output can be used for cosine similarity.

###### Input File - AirBnbData.csv
###### Output File - AirbnbChicagoOutput.csv

#### CosineSimilarity.py

Now that we have every parameter in list format, we are running cosine similarity for every independent column corresponding to Description using word2vec method.

###### Input File - AirbnbChicagoOutput.csv
###### Output File - AirbnbChicagoOutput.csv

#### SentimentAnalysis.py

To computationally identifying and categorizing opinions expressed in a piece of text, we are performing sentimental analysis for Description column.

###### Input File - AirbnbChicagoOutput.csv
###### Output File - AirbnbChicagoOutput_Final.csv

#### After getting the final output from the python code, we are using Minitab to perform regressions. First regression, is about occupancy rate corresponding to length of description, it's sentiment and it's comprehensiveness. We can see comprehensiveness have higher significance, so to explore more we ran a regression model for every input variable seperately. Below are the output file's name from minitab for both:
Regression1.htm and Regression2.htm


## Documentation

[GITLAB Project](https://gitlab.com/rupalbilaiya/airbnbdescriptionanalysis)

