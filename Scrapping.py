import pandas as pd

data = pd.read_csv(r'listingsChicago.csv')
data.info()

urls = []
for i in range(len(data['id'])):
    urls.append('https://www.airbnb.com/rooms/'+str(data['id'][i]))

path = r"chromedriver"

import time
from bs4 import BeautifulSoup as bs
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
chrome_options = Options()
chrome_options.add_argument('--no-sandbox')
chrome_options.add_argument('--disable-dev-shm-usage')

driver = webdriver.Chrome(path, options=chrome_options)

checkin = []
checkout = []
pet = []
smoking = []
parties = []
url_id = []
description = []
count = -1

for url in urls:
    print(url)
    driver.get(url)

    SCROLL_PAUSE_TIME = 15

    last_height = driver.execute_script("return document.body.scrollHeight")

    while True:
        driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")

        time.sleep(SCROLL_PAUSE_TIME)

        new_height = driver.execute_script("return document.body.scrollHeight")

        if new_height == last_height:
            break

        last_height = new_height
    try:
        wait = WebDriverWait(driver, 30)
        # wait.until(tag.find_elements(By.CLASS_NAME, 'tags'))
        wait.until(EC.visibility_of_element_located((By.CLASS_NAME, "_9t39z0r")))
        url_id.append(url)
        count += 1
        checkin.append('')
        checkout.append('')
        pet.append('')
        smoking.append('')
        parties.append('')
        description.append('')
    except:
        print('')

    print(count)
    html = driver.page_source  # Get the html of the page

    soup = bs(html, 'html.parser')

    for things in soup.find_all('div', class_='cihcm8w dir dir-ltr'):
        for check in things.find_all('div', class_='i1303y2k dir dir-ltr'):
            for span in check.find_all('span'):
                if 'check-in:' in span.text.lower() or 'checkin:' in span.text.lower():
                    checkin[count] = span.text
                    print(span.text)
                if 'check-out:' in span.text.lower() or 'checkout:' in span.text.lower():
                    checkout[count] = span.text
                    print(span.text)
                if 'smoking' in span.text.lower():
                    print(span.text)
                    smoking[count] = span.text
                if 'pets' in span.text.lower():
                    print(span.text)
                    pet[count] = span.text
                if 'parties' in span.text.lower():
                    print(span.text)
                    parties[count] = span.text

    for desc in driver.find_elements(By.XPATH, '//*[@id="site-content"]/div[3]/div/div[2]/div[2]/button'):
        driver.execute_script("arguments[0].click();", desc)
        driver.switch_to.window(driver.window_handles[-1])
        time.sleep(5)
        new_html = driver.page_source

        soup = bs(new_html, 'html.parser')
        desc_str = ''
        for desc in soup.find_all('div', {'data-plugin-in-point-id': "DESCRIPTION_MODAL"}):
            for desc in soup.find_all('div', class_='_178zopn'):
                for span in desc.find_all('span'):
                    desc_str += span.text

        description[count] = desc_str

    print(len(url_id))
    print(len(checkin))
    print(len(checkout))
    print(len(pet))
    print(len(smoking))
    print(len(parties))
    print(len(description))


d = {'Check-in': checkin, 'Check-out': checkout, 'Pets': pet, 'Smoking': smoking, 'Parties': parties, 'Description': description, 'url': url_id}
df = pd.DataFrame(d)

df.to_csv(r'AirbnbChicagoOutput.csv')

driver.quit()
