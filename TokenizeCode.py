import nltk
nltk.download('all')
from nltk.corpus import stopwords

from nltk.tokenize import sent_tokenize
from nltk.tokenize import word_tokenize
import nltk
nltk.download('punkt')

import pandas as pd
df = pd.read_csv(r'AirBnbData.csv', encoding="unicode_escape")
Descriptionkeyword = []
Neighbourhoodkeyword = []
ListD = list(df['Description'])
ListN = list(df['Neighbourhood_Overview'])

from nltk.corpus import stopwords
import nltk
nltk.download('stopwords')

import string
stopwords_list = stopwords.words('english') + list(string.punctuation)

for i,j in zip(ListD, ListN):
    word_list = word_tokenize(i)
    word_list1 = word_tokenize(j)
    words_to_keep_list = [word for word in word_list if word.lower() not in stopwords_list]
    words_to_keep_list1 = [word for word in word_list1 if word.lower() not in stopwords_list]
    Descriptionkeyword.append(words_to_keep_list)
    Neighbourhoodkeyword.append(words_to_keep_list1)

df['Description keyword'] = Descriptionkeyword
df['Neighbourhood keyword'] = Neighbourhoodkeyword
df.to_csv('AirbnbChicagoOutput.csv', index=False)