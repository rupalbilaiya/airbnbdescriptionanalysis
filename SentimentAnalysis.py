import pandas as pd
df = pd.read_csv(r'AirbnbChicagoOutput.csv')
sentiment = []
length = []
Descriptionkeyword = list(df['Description keyword'])
for i in Descriptionkeyword:
    k = len(i)
    length.append(k)
df['Length of Description'] = length
from nltk.sentiment import SentimentIntensityAnalyzer
sia = SentimentIntensityAnalyzer()
for i in Descriptionkeyword:
    m = sia.polarity_scores(i)
    sentiment.append(m)
df['Sentiment'] = sentiment
df.to_csv(r'AirbnbChicagoOutput_Final.csv',index=False)